#include "..\include\TText.h"

int main()
{
    TTextLink::InitMemSystem(14);
    TText text;
    text.Read("input.txt");
    text.GoFirstLink();
    text.GoDownLink();
    text.DelDownSection();
    text.InsDownSection("Tut bil Oleg");

    TTextLink::MemCleaner(text);


    text.Print();
    text.Write("output.txt");
}